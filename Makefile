# build options
DEBUG=1
LIBNL_VERSION=tiny

CC=/home/victorcrimea/home2/work/openwrt-clear/openwrt/staging_dir/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mips-openwrt-linux-uclibc-gcc
LD=/home/victorcrimea/home2/work/openwrt-clear/openwrt/staging_dir/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mips-openwrt-linux-uclibc-ld
STRIP=/home/victorcrimea/home2/work/openwrt-clear/openwrt/staging_dir/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mips-openwrt-linux-uclibc-strip

BUILDROOT=/home/victorcrimea/home2/work/openwrt-clear/openwrt

NAME=sniffer
OBJS=						   \
	average.o				   \
	capture.o   \
	channel.o				   \
	conf_options.o				   \
	essid.o					   \
	ieee80211_util.o			   \
	ifctrl-nl80211.o			   \
	listsort.o				   \
	main.o					   \
	network.o				   \
	node.o					   \
	protocol_parser.o			   \
	protocol_parser_wlan.o			   \
	radiotap/radiotap.o			   \
	util.o					   \
	wext.o					   \
	wlan_util.o
LIBS= -lm
CFLAGS+= -Os -Wall -Wextra -g -I.
ifeq ($(DEBUG),1)
CFLAGS+=-DDO_DEBUG
endif

ifeq ($(LIBNL_VERSION),tiny)
LIBS+=-L$(BUILDROOT)/staging_dir/target-mips_34kc_uClibc-0.9.33.2/usr/lib -lnl-tiny
else
LIBS+=-lnl-3 -lnl-genl-3
endif

.PHONY: all check clean force

all: $(NAME)
	$(STRIP) sniffer
	cp sniffer ./build/sniffer

.objdeps.mk: $(OBJS:%.o=%.c)
	gcc -MM $^ >$@

-include .objdeps.mk

$(NAME): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

$(OBJS): .buildflags

# libnl-tiny header files somehow pollute, so just include them for this file:
ifctrl-nl80211.o:
	$(CC) -c $(CFLAGS) -I$(BUILDROOT)/package/libs/libnl-tiny/src/include -o $@ $<

check:
	sparse *.[ch]

clean:
	-rm -f *.o radiotap/*.o *~
	-rm -f $(NAME)
	-rm -f .buildflags

.buildflags: force
	echo '$(CFLAGS)' | cmp -s - $@ || echo '$(CFLAGS)' > $@
