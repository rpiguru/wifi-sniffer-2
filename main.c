/* horst - Highly Optimized Radio Scanning Tool
 *
 * Copyright (C) 2005-2015 Bruno Randolf (br1@einfach.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <err.h>
#include <sys/socket.h>
#include <net/if.h>
#include <stdarg.h>
#include <libgen.h>

#include "main.h"
#include "util.h"
#include "capture.h"
#include "protocol_parser.h"
#include "network.h"
#include "wlan_util.h"
#include "ieee80211_util.h"
#include "channel.h"
#include "node.h"
#include "essid.h"
#include "conf_options.h"
#include "ifctrl.h"


struct list_head nodes;
struct essid_meta_info essids;
struct history hist;
struct statistics stats;
struct channel_info spectrum[MAX_CHANNELS];
struct node_names_info node_names;

struct config conf;

struct timeval the_time;

int mon; /* monitoring socket */

static FILE* DF = NULL;

struct packet_light accumulator[MAX_ACCUMULATOR_SIZE]; //used for in-memory storage of packets
int packets_accumulated=0;
int number_of_probes=0;
int whitelist_len=0;
int blacklist_len=0;
static int timeframe=0;
const char *real_mac;
/* receive packet buffer
 *
 * due to the way we receive packets the network (TCP connection) we have to
 * expect the reception of partial packet as well as the reception of several
 * packets at one. thus we implement a buffered receive where partially received
 * data stays in the buffer.
 *
 * we need two buffers: one for packet capture or receiving from the server and
 * another one for data the clients sends to the server.
 *
 * not sure if this is also an issue with local packet capture, but it is not
 * implemented there.
 *
 * size: max 80211 frame (2312) + space for prism2 header (144)
 * or radiotap header (usually only 26) + some extra */
static unsigned char buffer[2312 + 200];
static size_t buflen;

/* for packets from client to server */
static unsigned char cli_buffer[500];
static size_t cli_buflen;

/* for select */
static fd_set read_fds;
static fd_set write_fds;
static fd_set excpt_fds;

static volatile sig_atomic_t is_sigint_caught;

static int is_interface_added;

void __attribute__ ((format (printf, 1, 2))) printlog(const char *fmt, ...)
{
#if DO_DEBUG
	char buf[128];
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(&buf[1], 127, fmt, ap);
	va_end(ap);
	fprintf(stdout, "%s\n", &buf[1]);
#endif

}




void
update_spectrum_durations(void)
{
	/* also if channel was not changed, keep stats only for every channel_time.
	 * display code uses durations_last to get a more stable view */
	if (conf.channel_idx >= 0) {
		spectrum[conf.channel_idx].durations_last =
				spectrum[conf.channel_idx].durations;
		spectrum[conf.channel_idx].durations = 0;
		ewma_add(&spectrum[conf.channel_idx].durations_avg,
			 spectrum[conf.channel_idx].durations_last);
	}
}


static void 
write_to_file(struct packet_info* p)
{
	static bool first=true;
//	char buf[40];
//	int i;
	//struct tm* ltm = localtime(&the_time.tv_sec);
	if(first){
		first = !first;
	}else{
		fprintf(DF, "\\n");
	}
	fprintf(DF, "%d,", (int)the_time.tv_sec);
	fprintf(DF, "%s,", ether_sprintf(p->wlan_src));
	fprintf(DF, "%d", p->phy_signal);
	fflush(DF);
	packets_accumulated++;

}


/* return 1 if packet is filtered */
static int filter_packet(struct packet_info* p) {
	int i;

	if (conf.filter_pkt != PKT_TYPE_ALL && (p->pkt_types & ~conf.filter_pkt)) {
		stats.filtered_packets++;
		return 1;
	}

	/* cannot trust anything if FCS is bad */
	if (p->phy_flags & PHY_FLAG_BADFCS)
		return 1;

	if (p->wlan_mode!= WLAN_MODE_PROBE || p->wlan_mode == 0) {
		return 1;
	}

	if(blacklist_len > 0){
		for (i = 0; i < blacklist_len; i++) {
			printlog("I'm blacklisting");
			if (MAC_NOT_EMPTY(p->wlan_src) && memcmp(p->wlan_src, (void*)&blacklist[i], 6) == 0) {
				return 1;
			}
		}
	}

	if(whitelist_len > 0){
		for (i = 0; i < whitelist_len; i++) {
			if (MAC_NOT_EMPTY(p->wlan_src) && memcmp(p->wlan_src, (void*)&whitelist[i], 3) == 0) {
				printlog("I'm whitelisting");
				return 0;
			}
		}
		return 1; // return 1 will happen only in case MAC address is not in the whitelist
	}
	return 0;
}

void fixup_packet_channel(struct packet_info* p) {
	int i = -1;

	/* get channel index for packet */
	if (p->phy_freq) {
		i = channel_find_index_from_freq(p->phy_freq);
	}

	/* if not found from pkt, best guess from config but it might be
	 * unknown (-1) too */
	if (i < 0)
		p->pkt_chan_idx = conf.channel_idx;
	else
		p->pkt_chan_idx = i;

	/* wlan_channel is only known for beacons and probe response,
	 * otherwise we set it from the physical channel */
	if (p->wlan_channel == 0 && p->pkt_chan_idx >= 0)
		p->wlan_channel = channel_get_chan_from_idx(p->pkt_chan_idx);

	/* if current channel is unknown (this is a mac80211 bug), guess it from
	 * the packet */
	if (conf.channel_idx < 0 && p->pkt_chan_idx >= 0)
		conf.channel_idx = p->pkt_chan_idx;
}

void send_packets(){
	char curl_buffer[MAX_CURL_BUFFER];
	char curl_buffer_final[MAX_CURL_BUFFER];
	int curl_buffer_index=0;
	char entry_buffer[MAX_STRING_LEN*3+2];
	char time[MAX_STRING_LEN];
	char src[MAX_STRING_LEN];
	char rssi[MAX_STRING_LEN];
	char number[MAX_STRING_LEN];

	if(packets_accumulated==0){
		return;
	}

	if (conf.dumpfile[0] != '\0'){ //all temp data will be stored in file
		char curl_string[MAX_CURL_STRING];
		//writing AP mac to the begging of the curl_buffer
		snprintf(curl_string, MAC_STRING_LEN+2, "%s\n", real_mac);
		#ifdef DO_DEBUG
			sprintf(curl_string, "curl -s -H \"X-Sniffer-Version: %d\" -H \"X-Ap: %s\" -H \"X-Debug: 1\" -H \"Content-Type: text/plain\" -POST -d @%s %s", VERSION, real_mac, conf.dumpfile, conf.api_consume);
		#else
			sprintf(curl_string, "curl -s -H \"X-Sniffer-Version: %d\" -H \"X-Ap: %s\" -H \"Content-Type: text/plain\" -POST -d @%s %s >> /dev/null", VERSION, real_mac, conf.dumpfile, conf.api_consume);
		#endif
		system(curl_string); // CURL invocation
		dumpfile_open(conf.dumpfile);
	}
	else {
		int i;
		memset(curl_buffer, '\0', MAX_CURL_BUFFER);
		//writing AP mac to the begging of the curl_buffer
		snprintf(curl_buffer, MAC_STRING_LEN+2, "%s\n", real_mac);
		for(i=0; i<packets_accumulated; i++){
			if(MAX_CURL_BUFFER-curl_buffer_index<MAX_STRING_LEN*3+2) {
				break; // in this case next entry may not fit to the rest of the curl_buffer
			}
			snprintf(time,MAX_STRING_LEN,"%d", accumulator[i].time);
			snprintf(src,MAX_STRING_LEN,"%s", accumulator[i].src);
			snprintf(rssi,MAX_STRING_LEN,"%d", accumulator[i].signal);
			snprintf(number,MAX_STRING_LEN,"%d", accumulator[i].number);
			if(i==0){
				snprintf(entry_buffer, MAX_STRING_LEN*3+2, "%s,%s,%s,%s", time, src, rssi, number);
			}
			else{
				snprintf(entry_buffer, MAX_STRING_LEN*3+2, "\\n%s,%s,%s,%s", time, src, rssi, number);
			}
			strncat( curl_buffer, entry_buffer, MAX_STRING_LEN*3+2);
			curl_buffer_index+= strlen(entry_buffer);
			//Memory clean-up!
			free(accumulator[i].src);
		}

		#ifdef DO_DEBUG
			sprintf(curl_buffer_final, "curl -s -H \"X-Sniffer-Version: %d\" -H \"X-Ap: %s\" -H \"X-Debug: 1\" -H \"Content-Type: text/plain\" -POST -d \"%s\" %s", VERSION, real_mac, curl_buffer, conf.api_consume);
		#else
			sprintf(curl_buffer_final, "curl -s -H \"X-Sniffer-Version: %d\" -H \"X-Ap: %s\"  -H \"Content-Type: text/plain\" -POST -d \"%s\" %s >> /dev/null", VERSION, real_mac, curl_buffer, conf.api_consume);
		#endif
		system(curl_buffer_final); // CURL invocation
		printlog("\n %s \n", curl_buffer_final);
		printlog("packet_accumulated: %d", packets_accumulated);
		printlog("Number of probes: %d", number_of_probes);
		printlog("curl_buffer_index: %d", curl_buffer_index);
	}
	printlog("DATA SENT!");
	bzero(accumulator,MAX_ACCUMULATOR_SIZE*sizeof(struct packet_light));
	packets_accumulated=0;
	number_of_probes=0;
	timeframe = conf.timeframe_limit+(rand()%(conf.timeframe_dispersion*2+1)-conf.timeframe_dispersion);
	printf("timeframe: %d", timeframe);
}


void keep_in_memory(struct packet_info* p){
	struct packet_light packet;
	packet.time=(int)the_time.tv_sec;
	packet.src = ether_sprintf(p->wlan_src);
	packet.signal = p->phy_signal;
	packet.number=1;

	int i;
	for(i=0; i< packets_accumulated; i++){
		if(memcmp(accumulator[i].src, packet.src, 12) == 0){
			//Not the first time
			printlog("accumulator_mac: %s", accumulator[i].src);
			printlog("packet_mac: %s", packet.src);
			accumulator[i].signal = (accumulator[i].signal * accumulator[i].number + packet.signal)/++(accumulator[i].number);
			return;
		}
	}
	printlog("first time!");
	accumulator[packets_accumulated] = packet;
	packets_accumulated++;
}

void convert_string_to_mac_mask(const char *string, struct mac_mask *mask){
	int x = 0;
	char x1_str[3];
	char x3_str[3];
	char x2_str[3];
	memset(x1_str,'\0', sizeof(x1_str));
	memset(x2_str,'\0', sizeof(x2_str));
	memset(x3_str,'\0', sizeof(x3_str));

	strncpy ( x1_str, string, 2);
	strncpy ( x2_str, string+2, 2);
	strncpy ( x3_str, string+4, 2);

	if (strlen(string)==6) {
		sscanf(x1_str, "%x", &x);
		mask->x1=x;
		sscanf(x2_str, "%x", &x);
		mask->x2=x;
		sscanf(x3_str, "%x", &x);
		mask->x3=x;
	}
}

void str2mac(const char *string, struct mac *mac){
	int x = 0;
	char x1_str[3];
	char x2_str[3];
	char x3_str[3];
	char x4_str[3];
	char x5_str[3];
	char x6_str[3];
	memset(x1_str,'\0', sizeof(x1_str));
	memset(x2_str,'\0', sizeof(x2_str));
	memset(x3_str,'\0', sizeof(x3_str));
	memset(x4_str,'\0', sizeof(x3_str));
	memset(x5_str,'\0', sizeof(x3_str));
	memset(x6_str,'\0', sizeof(x3_str));

	strncpy ( x1_str, string, 2);
	strncpy ( x2_str, string+2, 2);
	strncpy ( x3_str, string+4, 2);
	strncpy ( x4_str, string+6, 2);
	strncpy ( x5_str, string+8, 2);
	strncpy ( x6_str, string+10, 2);

	if (strlen(string)==12) {
		sscanf(x1_str, "%x", &x);
		mac->x1=x;
		sscanf(x2_str, "%x", &x);
		mac->x2=x;
		sscanf(x3_str, "%x", &x);
		mac->x3=x;
		sscanf(x4_str, "%x", &x);
		mac->x4=x;
		sscanf(x5_str, "%x", &x);
		mac->x5=x;
		sscanf(x6_str, "%x", &x);
		mac->x6=x;
	}
}

/**
 * Downloads whitelist page from specified URL and creates array of whitelisted MAC addr prefixes
 * @brief get_whitelist
 */
void get_whitelist(){
	const int mask_len=7;
	char data[mask_len];
	char invoke_string[MAX_CURL_STRING];

	whitelist_len=0;

	//Check if whitelist is set
	if(strlen(conf.api_whitelist)<4){ //i'm assuming that api_whitelist is incorrect if it's less than 4 charachters long
		return;
		// whitelist_len is still ZERO
	}

	snprintf(invoke_string,MAX_CURL_STRING, "%s %s -s", "curl", conf.api_whitelist );
	printlog("GOT WHITELIST");
	FILE *fp = popen(invoke_string,"r");
	while (fgets(data,sizeof(data),fp) != NULL) {
		if(strlen(data)!=6){
			continue;
		}
		struct mac_mask temp_mask;
		convert_string_to_mac_mask(data,&temp_mask);
		whitelist[whitelist_len++]=temp_mask;
	}
}
void get_blacklist(){
	const int mac_len=13;
	char data[mac_len];
	char invoke_string[MAX_CURL_STRING];

	blacklist_len=0;

	//Check if blacklist is set
	if(strlen(conf.api_blacklist)<4){ //i'm assuming that api_blacklist is incorrect if it's less than 4 charachters long
		return;
		// blacklist_len is still ZERO
	}

	snprintf(invoke_string,MAX_CURL_STRING, "%s %s -s", "curl", conf.api_blacklist );
	printlog("GOT BLACKLIST");
	FILE *fp = popen(invoke_string,"r");
	while (fgets(data,sizeof(data),fp) != NULL) {
		if(strlen(data)!=12){
			continue;
		}
		struct mac temp_mac;
		str2mac(data,&temp_mac);

		blacklist[blacklist_len++]=temp_mac;
	}
}



void save_packet(struct packet_info* p){
	if (conf.dumpfile[0] != '\0'){ //all temp data will be stored in file
		write_to_file(p);
	}
	else {
		keep_in_memory(p);  //all temp data will be stored in memory
	}
	number_of_probes++;
}

int time_start;
int blacklist_recheck_time_start;
int whitelist_recheck_time_start;
void handle_packet(struct packet_info* p)
{
	struct node_info* n = NULL;
	int seconds_elapsed = the_time.tv_sec - time_start;

	/* filter on server side only */
	if (filter_packet(p)) {
		return;
	}

	fixup_packet_channel(p);

	if(packets_accumulated<conf.accumualtor_size && seconds_elapsed<timeframe){
			save_packet(p);
	}else {
		//here packets will be sent to server
		send_packets();
		time_start = the_time.tv_sec;
	}

	printlog("%d devices", packets_accumulated);
	printlog("%d probes", number_of_probes);
	printlog("%d elapsed_time", seconds_elapsed);

	if (!(p->phy_flags & PHY_FLAG_BADFCS)) {
		/* we can't trust any fields except phy_* of packets with bad FCS,
		 * so we can't do all this here */
		n = node_update(p);

		if (n)
			p->wlan_retries = n->wlan_retries_last;

		p->pkt_duration = ieee80211_frame_duration(
				p->phy_flags & PHY_FLAG_MODE_MASK,
				p->wlan_len, p->phy_rate,
				p->phy_flags & PHY_FLAG_SHORTPRE,
				0 /*shortslot*/, p->wlan_type,
				p->wlan_qos_class,
				p->wlan_retries);
	}

}


static void
local_receive_packet(int fd, unsigned char* buffer, size_t bufsize)
{
	int len;
	struct packet_info p;

	len = recv_packet(fd, buffer, bufsize);

	//Clean packet structure
	memset(&p, 0, sizeof(p));

	if (!parse_packet(buffer, len, &p)) {
		DEBUG("parsing failed\n");
		return;
	}

	handle_packet(&p);
}


static void receive_any(const sigset_t *const waitmask){
	int ret, mfd;
	long usecs;
	struct timespec ts;
	int seconds_elapsed = the_time.tv_sec - time_start;
	int seconds_from_last_whitelist_recheck = the_time.tv_sec - whitelist_recheck_time_start;
	int seconds_from_last_blacklist_recheck = the_time.tv_sec - blacklist_recheck_time_start;

	if(seconds_from_last_blacklist_recheck > conf.blacklist_recheck_time){
		get_blacklist();
		blacklist_recheck_time_start=the_time.tv_sec;
	}

	if(seconds_from_last_whitelist_recheck > conf.whitelist_recheck_time){
		get_whitelist();
		whitelist_recheck_time_start=the_time.tv_sec;
	}


	if(seconds_elapsed>=timeframe){
		send_packets();
		packets_accumulated=0;
		time_start = the_time.tv_sec;
	}
	FD_ZERO(&read_fds);
	FD_ZERO(&write_fds);
	FD_ZERO(&excpt_fds);
//	if (!conf.quiet && !conf.debug)
//		FD_SET(0, &read_fds);
	FD_SET(mon, &read_fds);
	if (srv_fd != -1)
		FD_SET(srv_fd, &read_fds);
	if (cli_fd != -1)
		FD_SET(cli_fd, &read_fds);
	//if (ctlpipe != -1)
		//FD_SET(ctlpipe, &read_fds);

	usecs = max(0, min(channel_get_remaining_dwell_time(), 1000000));
	ts.tv_sec = usecs / 1000000;
	ts.tv_nsec = usecs % 1000000 * 1000;
	mfd = max(mon, srv_fd);
//	mfd = max(mfd, ctlpipe);
	mfd = max(mfd, cli_fd) + 1;

	ret = pselect(mfd, &read_fds, &write_fds, &excpt_fds, &ts, waitmask);
	if (ret == -1 && errno == EINTR) /* interrupted */
		return;
	if (ret == 0) { /* timeout */
//		if (!conf.quiet && !conf.debug)
//			update_display_clock();
		return;
	}
	else if (ret < 0) /* error */
		err(1, "select()");

	/* stdin */
//	if (FD_ISSET(0, &read_fds) && !conf.quiet && !conf.debug)
//		handle_user_input();

	/* local packet or client */
	if (FD_ISSET(mon, &read_fds)) {
		if (conf.serveraddr[0] != '\0')
			net_receive(mon, buffer, &buflen, sizeof(buffer));
		else
			local_receive_packet(mon, buffer, sizeof(buffer));
	}

	/* server */
	if (srv_fd > -1 && FD_ISSET(srv_fd, &read_fds))
		net_handle_server_conn();

	/* from client to server */
	if (cli_fd > -1 && FD_ISSET(cli_fd, &read_fds))
		net_receive(cli_fd, cli_buffer, &cli_buflen, sizeof(cli_buffer));

	/* named pipe */
	//if (ctlpipe > -1 && FD_ISSET(ctlpipe, &read_fds))
		//control_receive_command();
}


void
free_lists(void)
{
//	int i;
	struct essid_info *e, *f;
	struct node_info *ni, *mi;
	//struct chan_node *cn, *cn2;

	/* free node list */
	list_for_each_safe(&nodes, ni, mi, list) {
		list_del(&ni->list);
		free(ni);
	}

	/* free essids */
	list_for_each_safe(&essids.list, e, f, list) {
		//DEBUG("free essid '%s'\n", e->essid);
		list_del(&e->list);
		free(e);
	}

}


static void
exit_handler(void)
{
	free_lists();

	//if (!conf.serveraddr[0] != '\0')
		//close_packet_socket(mon, conf.ifname);

	if (is_interface_added) {
		ifctrl_ifdown(conf.ifname);
		ifctrl_iwdel(conf.ifname);
	}

	if (DF != NULL) {
		fclose(DF);
		DF = NULL;
	}

	//if (conf.allow_control)
		//control_finish();

	//if (!conf.debug)
		//net_finish();

//	if (!conf.quiet && !conf.debug)
//		finish_display();
}


static void
sigint_handler(__attribute__((unused)) int sig)
{
	/* Only set an atomic flag here to keep processing in the interrupt
	 * context as minimal as possible (at least all unsafe functions are
	 * prohibited, see signal(7)). The flag is handled in the mainloop. */
	is_sigint_caught = 1;
}


static void
sigpipe_handler(__attribute__((unused)) int sig)
{
	/* ignore signal here - we will handle it after write failed */
}


void
init_spectrum(void)
{
	int i;

	for (i = 0; i < MAX_CHANNELS; i++) {
		list_head_init(&spectrum[i].nodes);
		ewma_init(&spectrum[i].signal_avg, 1024, 8);
		ewma_init(&spectrum[i].durations_avg, 1024, 8);
	}
}



static void generate_mon_ifname(char *const buf, const size_t buf_size)
{
	unsigned int i;

	for (i=0;; ++i) {
		int len;

		len = snprintf(buf, buf_size, "horst%d", i);
		if (len < 0)
			err(1, "failed to generate a monitor interface name");
		if ((unsigned int) len >= buf_size)
			errx(1, "failed to generate a sufficiently short "
			     "monitor interface name");
		if (!if_nametoindex(buf))
			break;
	}
}

int main(int argc, char** argv) {
	char buffer[BUFSIZ];
	readlink("/proc/self/exe", buffer, BUFSIZ);
	char *currDir;
	currDir = dirname(buffer);
	//printf("%s\n", currDir);

	//Generate random seed
	srand(time(NULL));

	//=======Real Mac=====
	FILE *mac_file;
	mac_file = fopen("/sys/class/net/br-lan1/address","r");
	real_mac = (char*)malloc(sizeof(char)*MAC_STRING_LEN);

	if(mac_file == NULL) {
		real_mac = ether_sprintf(conf.my_mac_addr);
	}else{
		fgets(real_mac,MAC_STRING_LEN, mac_file);
	}
	printlog("AP_mac: %s", real_mac);

	sigset_t workmask;
	sigset_t waitmask;
	struct sigaction sigint_action;
	struct sigaction sigpipe_action;

	list_head_init(&essids.list);
	list_head_init(&nodes);
	//init_spectrum();

	config_parse_file_and_cmdline(argc, argv);

	get_whitelist();
	get_blacklist();

	timeframe = conf.timeframe_limit + (rand()%(conf.timeframe_dispersion*2+1));

	//Exit handlers===============================
	sigint_action.sa_handler = sigint_handler;
	sigemptyset(&sigint_action.sa_mask);
	sigint_action.sa_flags = 0;
	sigaction(SIGINT, &sigint_action, NULL);
	sigaction(SIGTERM, &sigint_action, NULL);
	sigaction(SIGHUP, &sigint_action, NULL);

	sigpipe_action.sa_handler = sigpipe_handler;
	sigaction(SIGPIPE, &sigpipe_action, NULL);

	atexit(exit_handler);
	//============================================


	gettimeofday(&stats.stats_time, NULL);
	gettimeofday(&the_time, NULL);

	conf.channel_idx = -1;

//	if (conf.mac_name_lookup)
		//mac_name_file_read(conf.mac_name_file);

	if (conf.serveraddr[0] != '\0')
		mon = net_open_client_socket(conf.serveraddr, conf.port);
	else {
		/* Try to set the interface to monitor mode or create a virtual
		 * monitor interface as a fallback. */
		if (ifctrl_iwset_monitor(conf.ifname)) {
			char mon_ifname[IF_NAMESIZE];

			warnx("failed to set interface '%s' to monitor mode, "
			      "adding a virtual monitor interface",
			      conf.ifname);

			generate_mon_ifname(mon_ifname, IF_NAMESIZE);
			if (ifctrl_iwadd_monitor(conf.ifname, mon_ifname))
				err(1, "failed to add a virtual monitor "
				    "interface");

			printlog("INFO: A virtual interface '%s' will be used "
				 "instead of '%s'.", mon_ifname, conf.ifname);
			config_handle_option(0, "interface", mon_ifname);
			is_interface_added = 1;
			/* Now we have a new monitor interface, proceed
			 * normally. The interface will be deleted at exit. */
		}

		if (ifctrl_ifup(conf.ifname))
			err(1, "failed to bring interface '%s' up",
			    conf.ifname);

		mon = open_packet_socket(conf.ifname, conf.recv_buffer_size);
		if (mon <= 0)
			err(1, "Couldn't open packet socket");
		conf.arphrd = device_get_hwinfo(mon, conf.ifname,
						conf.my_mac_addr);

		if (conf.arphrd != ARPHRD_IEEE80211_PRISM &&
		    conf.arphrd != ARPHRD_IEEE80211_RADIOTAP)
			err(1, "interface '%s' is not in monitor mode",
			    conf.ifname);

		if (!channel_init() && conf.quiet)
			err(1, "failed to change the initial channel number");
	}

	/* Race-free signal handling:
	 *   1. block all handled signals while working (with workmask)
	 *   2. receive signals *only* while waiting in pselect() (with waitmask)
	 *   3. switch between these two masks atomically with pselect()
	 */
	if (sigemptyset(&workmask)                       == -1 ||
	    sigaddset(&workmask, SIGINT)                 == -1 ||
	    sigaddset(&workmask, SIGHUP)                 == -1 ||
	    sigaddset(&workmask, SIGTERM)                == -1 ||
	    sigprocmask(SIG_BLOCK, &workmask, &waitmask) == -1)
		err(1, "failed to block signals: %m");

	time_start = the_time.tv_sec;
	while(1){
		receive_any(&waitmask);
		if (is_sigint_caught)
			exit(1);

		gettimeofday(&the_time, NULL);
		timeout_nodes();
	}
	return 0;
}


void
main_pause(int pause)
{
	conf.paused = pause;
	printlog(conf.paused ? "- PAUSED -" : "- RESUME -");
}


void main_reset()
{

	printlog("- RESET -");
	free_lists();
	essids.split_active = 0;
	essids.split_essid = NULL;
	memset(&hist, 0, sizeof(hist));
	memset(&stats, 0, sizeof(stats));
	memset(&spectrum, 0, sizeof(spectrum));
	init_spectrum();
	gettimeofday(&stats.stats_time, NULL);
}


void
dumpfile_open(const char* name)
{
	if (DF != NULL) {
		fclose(DF);
		DF = NULL;
	}

	if (name == NULL || strlen(name) == 0) {
		printlog("- Not writing outfile");
		conf.dumpfile[0] = '\0';
		return;
	}

	strncpy(conf.dumpfile, name, MAX_CONF_VALUE_STRLEN);
	conf.dumpfile[MAX_CONF_VALUE_STRLEN] = '\0';
	DF = fopen(conf.dumpfile, "w");
	if (DF == NULL)
		err(1, "Couldn't open dump file");

	//fprintf(DF, "time,mac_src,rssi\n");
	printlog("- Writing to outfile %s", conf.dumpfile);
}
